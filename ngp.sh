#!/bin/bash

# ngp - new git project - criação e configuração inicial de projeto no GitLab

# Arkanon <arkanon@lsd.org.br>
# 2024/10/04 (Fri) 15:12:37 -03
# 2024/05/07 (Tue) 05:16:08 -03
# 2023/12/04 (Mon) 23:35:33 -03
# 2022/06/24 (Fri) 15:52:29 -03
# 2021/10/17 (Sun) 02:06:28 -03
# 2021/04/29 (Thu) 15:40:56 -03
# 2020/12/13 (Sun) 23:20:54 -03
# 2020/09/11 (Fri) 22:48:27 -03
# 2020/03/17 (Tue) 19:28:49 -03
# 2019/10/18 (Fri) 15:56:12 -03
# 2019/10/17 (Thu) 10:42:12 -03
# 2019/10/16 (Wed) 11:19:58 -03
# 2019/09/17 (Tue) 10:32:46 -03
# 2019/09/11 (Wed) 11:40:22 -03
# 2019/08/27 (Tue) 16:52:13 -03
# 2019/08/22 (Thu) 15:14:18 -03



# Sumário
#
# -- inclusão da biblioteca
# -- inclusão  dos parâmetros mais padrão
# -- definição dos parâmetros especificos
# -- instalação de ferramentas auxiliares
# -- criação do projeto
# -- clonagem do projeto
# -- criação dos arquivos de controle
# -- README.md inicial
# -- configuração do avatar (icone)
# -- configuração da identidade do usuário para acesso ao repositório remoto
# -- criação da branch master
# -- criação de branch extra
# -- commit inicial



  cffile=$HOME/.gitlib



  . gitlib.sh                             # biblioteca
  [ -e $cffile       ] && . $cffile       # arquivo de configuração global pessoal
  [ -e ./gitlib.conf ] && . ./gitlib.conf # arquivo de configuração específico



  if [[ $# == 0 || $* == do ]]
  then
    profiles=$(grep -oP 'profile-\K[^ (]+(?=\()' $cffile | sed 's/^/  /')
    echo -e "\nPerfil: $profile"
    echo -e "\nIdentidade: $identity"
    echo -e "\nPerfis:\n$profiles"
    echo -e "\nUso: [profile=<nome_perfil>] ${0##*/} [namespace/]<projeto>[:branch] [caminho_icone] [descrição] [do]\n"
    echo -e "  -- o namespace default é o user do perfil ($gl_user). Pode ser um caminho na forma grupo/dir/dir/..."
    echo -e "  -- se nenhuma branch for indicada, apenas a master será criada;"
    echo -e "  -- caminho_icone de imagem jpg|png|svg. Uma url, se iniciar com http(s), ftp(s) ou 'gitlab:'."
    echo -e "     Se for gitlab, procurará no projeto <http://gitlab.com/arkanon/misc/logos>. Sem um diretório indicado, assumirá a extensão como diretório (jpg|png|svg);"
    echo -e "  -- na descrição, caracteres \""\'"!;() deve ser escapados;"
    echo -e "  -- o último parâmetro 'do' efetivamente cria o projeto. Sem ele, apenas são mostrados os parâmetros que serão utilizados após serem processados.\n"
    exit
  fi



  (( $(grep -o : <<< $1 | wc -l) > 1 )) && { echo "Apenas uma ocorrência de : é permitida, separando o nome do projeto do nome da branch extra."; exit; }



  # primeiro argumento é o "caminho" namespace/projeto:branch
  [[ $1 =~ / ]] && _namespace=${1%/*}
  [[ $1 =~ : ]] &&    _branch=${1##*:}
  _namespace=${_namespace:-$gl_user}
     _branch=${_branch:-$def_branch}
    _project=${1##*/}
    _project=${_project%:*}; shift

  [[ -d $_project ]] && { echo "Já exite um subdiretório \"$_project\" no diretório corrente ($PWD)."; exit; }

  # segundo argumento é o caminho do icone jpg|png|svg. Uma url se iniciar por http(s) ou ftp(s) ou gitlab: (nesse caso indicando um arquivo no diretório jpg|png|svg do projeto 'misc/logos')
  [[ $1 =~ ^gitlab: ]] && _icon_src=$1 || _icon_src=$(realpath $1); shift

  args=( "$@" )

  # último argumento indica a criação efetiva do projeto, se for "do"
  do=$( [[ $* && ${@: -1} == do ]] && echo true || echo false )
  $do && unset "args[${#args[@]}-1]"

  # quaisquer outros entre o terceiro e penúltimo (inclusive) são a descrição do projeto
  _description=${args[*]}



# >> ------------------------------------------------------------------------------------------------------------------- << #



  # >> instalação de ferramentas auxiliares <<

# #  qpdf  pdftk    # manipulação de pdf
# #  csvkit         # manipulação de csv  [ csvcut csvformat csvsort ]
# #  tidy           # manipulação de html/xhtml/xml
# #  libxml2-utils  # manipulação de xml  [ xmllint ]
# #  yq             # manipulação de yaml <http://mikefarah.gitbook.io/yq>
  #  jq             # manipulação de json <http://lzone.de/cheat-sheet/jq>
  #  git-lfs        # gerenciamneto inteligente de arquivos binários pelo git
  #  librsvg2-bin   # rsvg-convert

  msg="$msg$( test_pkg curl         curl         I )"
  msg="$msg$( test_pkg jq           jq           I )"
  msg="$msg$( test_pkg git          git          I )"
  msg="$msg$( test_pkg git-lfs      git-lfs      I )"
  msg="$msg$( test_pkg librsvg2-bin rsvg-convert I )"
  [[ $msg ]] && { echo -en "\n$msg"; exit; }

# deps="curl jq git-lfs librsvg2-bin"
# if ! dpkg -l $deps > /dev/null
# then
#   echo deb http://ppa.launchpad.net/rmescandon/yq/ubuntu bionic main | sudo tee /etc/apt/sources.list.d/rmescandon-ubuntu-yq-bionic.list
#   sudo apt install -y gnupg2
#   sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys $( apt update 2>&1 | grep -oP '( NO_PUBKEY \K[^ ]+)+' | sort -u )
#   sudo apt update
#   sudo apt -y install $deps
# fi



  api_token=$(cat $api_token_file)



  if [[ $_icon_src ]]
  then

    found=false

    if [[ $_icon_src =~ ^gitlab:|(ht|f)tps?:// ]]
    then

      irepo=arkanon%2Fmisc%2Flogos
        dir=${_icon_src#*:}
        dir=${dir%/*}
        dir=${dir////%2F}
       file=${_icon_src##*:}
       file=${file##*/}
        ext=${file##*.}
       [[ ! $dir || $dir == $file ]] && dir=$ext

    #  curl=$(
    #          grep -qE '^gitlab:' <<< $_icon_src \
    #          && echo api1 GET /projects/$irepo/repository/files/$dir%2F$file/raw?ref=master \
    #          || echo curl -sL $_icon_src
    #        )
    # (
    #      apiurl=https://gitlab.com/api/v4 #
    #   api_token=$(<$adm_dir/.gl-pb)
    #   $curl -o ${icon%.*}.$ext
    # )

      grep -qE '^gitlab:' <<< $_icon_src && _icon_src=https://gitlab.com/api/v4/projects/$irepo/repository/files/$dir%2F$file/raw?ref=master

      curl -fsI $_icon_src >& /dev/null && found=true

    else

       file=${_icon_src##*/}
        ext=${file##*.}

      [[ -e $_icon_src ]] && found=true

    fi

    $found || { echo -e "Ícone não encontrado em [$_icon_src]." >&2; exit 1; }

  fi



  cat << EOT

_namespace   = $_namespace
_project     = $_project
_branch      = $_branch
_icon_src    = $_icon_src
_description = $_description

EOT
  $do || exit



  # >> criação do projeto << <http://docs.gitlab.com/ce/api/projects.html#list-a-user-s-projects>

  if [[ $_namespace != $gl_user ]]
  then
    id=$(api1 GET /namespaces/"${_namespace////%2F}" | jq .id)
    [[ $id == null ]] && { echo -e "Namespace não encontrado.\n" >&2; exit 1; }
    ns="-F namespace_id=$id"
  fi
  api1 POST /projects $ns -F name="$_project" -F description="$_description"
  # >> clonagem do projeto <<
  git clone "git@$gl_host:$_namespace/$_project"



  mkdir -p $_project

  if cd $_project
  then



    # >> criação do projeto <<

  # git init # >> se a criação do projeto a partir do push abaixo não funcionar, criar pela api com o comando acima e comentar este git init <<

    # configuração da identidade do usuário para acesso ao repositório remoto
    # BUG: deveria ter sido setada automaticamente pela config global já na clonagen usando o hook no diretório .git-templates
    git config user.name  "$user_name"
    git config user.email $user_email
    # OU
  # git identity $identity # se o alias identity estiver configurado em ~/.gitconfig

    touch README.md
    git add .
    git commit -m "criação do projeto"
    git remote add origin   git@$gl_host:$_namespace/$_project.git
    git push --set-upstream git@$gl_host:$_namespace/$_project.git $def_branch # válido a partir do gitlab 10.5 <http://docs.gitlab.com/ce/gitlab-basics/create-project.html#push-to-create-a-new-project>



    # >> descrição <<
    api1 PUT "/projects/${_namespace////%2F}%2F$_project" -F description="$_description"



    control_files



    # README.md
    echo -e "# $_project\n\n$_description" >| README.md



    # >> configuração do icone do projeto <<
    if [[ $_icon_src ]]
    then
      if [[ $_icon_src =~ ^gitlab:|(ht|f)tps?:// ]]
      then
        curl -sL $_icon_src -o ${icon%.*}.$ext
      else
        cp --preserve=all $_icon_src ${icon%.*}.$ext || { echo "Erro copiando o ícone." >&2; exit 1; }
      fi
      ls -la ${icon%.*}.*
      [[ $ext == svg ]] && rsvg-convert ${icon%.*}.svg -o $icon -w 512
      api1 PUT /projects/${_namespace////%2F}%2F$_project -F avatar=@$icon
    fi



    # >> commit de inicialização <<
    git add .
    git commit -m 'commit de inicialização'
    git push



    # >> criação de branch extra <<
    if [[ $_branch != $def_branch ]]
    then
      git checkout -b        $_branch
      git push     -u origin $_branch
      git pull
    fi



    git branch -a



  fi



# EOF
