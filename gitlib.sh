#!/bin/bash

# gitlib.sh

# Arkanon <arkanon@lsd.org.br>
# 2024/10/04 (Fri) 15:21:59 -03
# 2024/05/07 (Tue) 05:16:44 -03
# 2022/06/24 (Fri) 15:53:19 -03
# 2021/10/31 (Sun) 00:13:55 -03
# 2021/10/17 (Sun) 03:57:51 -03
# 2020/12/13 (Sun) 23:20:39 -03
# 2020/06/18 (Thu) 01:53:28 -03
# 2020/06/02 (Tue) 20:24:10 -03
# 2020/04/19 (Sun) 03:58:03 -03
# 2020/03/17 (Tue) 19:28:33 -03
# 2019/10/17 (Thu) 09:48:17 -03
# 2019/09/11 (Wed) 15:25:28 -03



# Sumário
#
# -- yaml2json
# -- edição de múltiplos arquivos em abas com nomes customizados no vi
# -- wrapper de nivel 1 da api (utiliza diretamente as urls da api)
# -- wrapper de nivel 2 da api (utiliza o wrapper de nivel 1)
# -- remoção automática dos arquivos auxiliares / repo local / repo remoto / projeto
# -- debug por inspeção das variáveis auxiliares e consulta através da API
# -- criação do token pessoal de API
# -- envio de chave SSH pública para o servidor GitLab
# -- criação dos arquivos de controle



  # yaml2json
  y2j() { ruby -ryaml -rjson -e 'puts JSON.pretty_generate(YAML.load(ARGF))' $* | jq . ; }



  test_pkg()
  {
    pkg=$1
    cmd=$2
    why=$3
    pkm=unsupported
    for i in apt-get apt yum
    {
      which $i &> /dev/null && pkm=$i
    }
    which $cmd &> /dev/null || \
    {
      [[ $pkm == unsupported ]] \
      && m="O formato de empacotamento do seu sistema não é deb ou rpm. ${why}nstale o pacote correspondente ao comando ${cmd##*/}." \
      || m="${why}nstale o pacote $pkg:\n\n  sudo $pkm -y install $pkg"
      echo "Comando '${cmd##*/}' não encontrado. $m\n\n"
    }
  }



  # >> edição de múltiplos arquivos em abas com nomes customizados no vi <<
  vi()
  {
    local f s m l t v # p
     f=$1
     s=${_V:-${f##*/}}
    _V=${_V:-VIM_$RANDOM} # não deve ser local para precisar ser setada apenas uma vez
    { grep -q -- -h <<< $* && [[   $_V ]]; } && m="\nVIm selecionado: ${_V^^}\n"
    { grep -q -- -h <<< $* || [[ ! $_V ]]; } && \
    {
      l=$(vim --serverlist | sed 's/^/  /')
      echo -e "$m\nVIM's já abertos:\n\n$l\n\nDefina um VIm com\n\n  _V=<label>\n"
      return
    }
    shift
    t=$(echo $*)
  # p=$([[ -e $f ]] && readlink -f $f || echo .)
    f=$([[    $f ]] && echo --remote-tab-silent $f)
    v="gvim -c 'set shortmess+=A' -c 'let &titlestring=\"$s\"' --servername $_V"
    eval "$v $f"
  # while [ ! -e ${p%/*}/.${p##*/}.swp ]; do sleep .1; done
    while ! vim --serverlist | grep -qi $_V; do sleep 1; done
    [[ $t ]] && eval "$v --remote-send ':TabooRename $t<cr>'"
  }
  export -f vi



  # >> wrapper de nivel 1 da api (utiliza diretamente as urls da api) <<
  api1()
  {
    local method epoint out
    method=$1; shift
    epoint=$1; shift
       out=$(
              curl -s \
                -X $method \
                -H PRIVATE-TOKEN:$api_token \
                -w "\n%{http_code}\n" \
                   $apiurl$epoint "$@"
            )
            # <http://iana.org/assignments/http-status-codes/http-status-codes.xhtml>
            #  1xx: information  - request received, continuing process
            #  2xx: success      - the action was successfully received, understood, and accepted
            #  3xx: redirection  - further action must be taken in order to complete the request
            #  4xx: client error - the request contains bad syntax or cannot be fulfilled
            #  5xx: server error - the server failed to fulfill an apparently valid request
            #
            #  200 ok
            #  201 created
            #  400 bad request
            #  404 not found
    # se $out começa com [ ou { assume que é json e formata com jq antes de devolver, caso contrário devolve direto
  # echo curl -s -X $method -H PRIVATE-TOKEN:$api_token -w "\n%{http_code}\n" $apiurl$epoint "$@"
    head -n-1 <<< $out | { cut -c1 <<< $out | grep -qE '[[{]' && jq . || cat; }
    httpc=$(tail -n1 <<< $out)
    [[ $httpc =~ 2.. ]]
    return $?
  }



  # wrapper de nivel 2 da api (utiliza o wrapper de nivel 1)
  api2var()
  {
    (( $# == 0 )) && { echo "Usage: $FUNCNAME [<namespace>/]<project> [<varname>[-]] <value>"; return 255; }
    local path key val out meth keyp
    path=/projects/${1//\//%2F}/variables
     key=$([[ ${2: -1} == - ]] && echo ${2::-1} || echo $2) # se o segundo parâmetros termina com '-', o nome da váriável é a palavra que vem antes, senão é o próprio parâmetro
    if [[ $key ]]
    then
      out=$(api1 GET $path/$key)
      sts=$?
      if [[ ${2: -1} == - ]]
      then
        api1 DELETE $path/$key
      else
        if [[ ! -z ${3+x} || $3 ]] # How to check if a variable is set in Bash? <http://stackoverflow.com/a/13864829>
        then
           val=$3
          meth=$((( $sts == 0 )) && echo PUT || echo POST)
          keyp=$((( $sts == 0 )) && echo /$key)
          api1 $meth $path$keyp \
            -F           key=$key \
            -F         value="$val" \
            -F variable_type=env_var \
            -F     protected=false \
            -F        masked=false
        # echo $?
        # echo $httpc
        else
          echo "$out" | jq .
        fi
      fi
    else
      api1 GET $path
    fi
  }



  # >> remoção automática dos arquivos auxiliares / repo local / repo remoto / projeto <<
  remove_all()
  {

    ssh-agent -k
    unset SSH_AGENT_PID
    unset SSH_AUTH_SOCK
    sudo -k

    # delete the project
    api1 DELETE /projects/$_namespace%2F$_project

    # delete the user ssh key
    api1 DELETE /user/keys/$(api1 GET /user/keys | jq '.[] | select(.title == "'$ssh_key_name'").id')

    # delete the personal access token
           out1=$( $curl https://$gl_host/users/sign_in )
    auth_token1=$( grep -ioP 'authenticity_token"? +value="?\K[^"]+' <<< $out1 )
           out2=$( $curl https://$gl_host/users/auth/ldapmain/callback --data-urlencode authenticity_token=$auth_token1 -d username=$gl_user -d password=$gl_pass )
           out3=$( $curl https://$gl_host/profile/personal_access_tokens )
    auth_token2=$( grep -ioP 'authenticity_token"? +value="?\K[^"]+' <<< $out3 )
     token_list=$( grep -ioP '<td>[^<].+|<a class="btn.+' <<< $out3 | sed 'N;N;N;s/\n//g' )
     token_href=$( grep ">$api_token_name<" <<< $token_list | grep -ioP 'href="\K[^"]+' )
           out4=$( $curl -X PUT https://$gl_host/$token_href --data-urlencode authenticity_token=$auth_token2 )

    # delete local files and project directory
    [[ -f "$cookie_file"    ]] && rm    $cookie_file
    [[ -f "$api_token_file" ]] && rm    $api_token_file
    [[ -f "$keyf"           ]] && rm    $keyf{,.p*}
    [[ -f "$keyp"           ]] && rm    $keyp
    [[ -d "$_project"       ]] && rm -r $_project

  }



  # >> debug por inspeção das variáveis auxiliares e consulta através da API <<
  debug()
  {

    $tidy <<< $out1
    $lynx <<< $out1
    $tidy <<< $out1 | grep -ioP '</?(form|input)[^>]*>' | grep --color -P '="\K[^"]+|'
    echo $auth_token1

    $tidy <<< $out2
    $lynx <<< $out2

    $tidy <<< $out3
    $lynx <<< $out3
    $tidy <<< $out3 | grep -ioP '</?(form|input)[^>]*>' | grep --color -P '="\K[^"]+|'
    echo $auth_token2

    $tidy <<< $out4
    $lynx <<< $out4
    $tidy <<< $out4 | grep -ioP '</?(form|input)[^>]*>' | grep --color -P '="\K[^"]+|'
    echo $api_token

    grep --color -P '>\K[^<]+|="\K[^"]+|' <<< $token_list

    api1 GET /user/keys
    ssh-add -l

    # teste da chave
  # opts="
  #        -o StrictHostKeyChecking=no
  #        -o UserKnownHostsFile=/dev/null
  #        -o LogLevel=quiet
  #      "
    opts="
           -o PreferredAuthentications=publickey
           -o NumberOfPasswordPrompts=0
         "
    ssh $opts -i $keyf git@$gl_host
    # PTY allocation request failed on channel 0
    # Welcome to GitLab, @arkanon!
    # Connection to gitlab.com closed.

    api1 GET  /users/$_namespace/projects | jq '.[]|{name,description}'

  }



  # >> criação do token pessoal de API << <http://gist.github.com/michaellihs/5ef5e8dbf48e63e2172a573f7b32c638>
  create_token()
  {

    # GET session cookie and source with auth token from login page
    out1=$( $curl https://$gl_host/users/sign_in )

    # scrape the auth token from user login page
    auth_token1=$( grep -ioP 'authenticity_token"? +value="?\K[^"]+' <<< $out1 )

    # POST login credentials, using cookies and token from previous request
    out2=$(
            $curl https://$gl_host/users/auth/ldapmain/callback \
              --data-urlencode authenticity_token=$auth_token1 \
              -d username=$gl_user \
              -d password=$gl_pass
          )

    # GET personal access token page to get auth token
    out3=$( $curl https://$gl_host/profile/personal_access_tokens )

    # scrape the new auth token from token page
    auth_token2=$( grep -ioP 'authenticity_token"? +value="?\K[^"]+' <<< $out3 )

    # POST generate personal access token form. The response will be a redirect, so follow using -L
    out4=$(
            $curl https://$gl_host/profile/personal_access_tokens \
              --data-urlencode authenticity_token=$auth_token2 \
              -d personal_access_token[name]=$api_token_name \
              -d personal_access_token[expires_at]= \
              -d personal_access_token[scopes][]=api
          )

    # scrape the personal access token from the response HTML
    api_token=$( grep -ioP 'created-personal-access-token"? +value="?\K[^"]+' <<< $out4 )
    echo $api_token >| $api_token_file
    chmod 600 $api_token_file

  }



  # >> envio de chave SSH pública para o servidor GitLab <<
  send_key()
  {

    mkdir -p ${keyf%/*}
    echo y | ssh-keygen -b 2048 -t rsa -f $keyf -C $comm -N "$keyp" # echo responde confirmação para sobrescrever, caso já exista
    puttygen --old-passphrase <(echo "$keyp") -o $keyf.ppk -C $comm $keyf

  # cat $keyf.pub | xclip -sel clip
  # # colar em <https://$gl_host/profile/keys>

    grep -q $keyf $sshc || cat << EOT >> $sshc

    Host $gl_host
      IdentityFile $keyf
      User $gl_user
      Port $gl_port
      ForwardX11 no

EOT

    # TODO: testar se já existe uma chave com o mesmo titulo
    api1 POST /user/keys -F title=$ssh_key_name -F key="$(cat $keyf.pub)"

    {
      [[ $SSH_AGENT_PID ]] && echo "Agente já em execução." || eval $(ssh-agent -s)
      expect="spawn ssh-add $keyf; expect \"Enter passphrase\"; send \"$keyp\n\"; expect \"Identity added\"; interact"
      fprint=$(ssh-keygen -lf $keyf | cut -d\  -f2)
      ssh-add -l | grep -qF $fprint && echo "Chave já armazenada." || expect <<< "$expect"
    }

  }



  # >> arquivos de controle <<
  control_files()
  {

    # .gitattributes (git lfs - large file support)
    typeset -A bintypes
    bintypes=(
                  [image]=png,gif,bmp,pcx,wmf,jpg,jpeg,jpe,ico,xcf,webp,cdr,cmx,ai,psd,dxf,wpg
                  [ebook]=pdf,pdb,epub,mobi,azw,azw3,iba,lrs,lrf,lrx,fb2,djvu
                 [office]=rtf,wps,doc,docx,docm,dot,dotx,dotm,xls,xlw,xlt,xlsx,xlsm,xlsb,xlts,xltm,ppt,pot,pptx,ptm,potx,potm,pps,ppsx,pub,vdx,vsd,vsdm,vsdx
                  [libre]=odt,ods,odp,odg,odb,odf,sxm,smf,ott,oth,odm,fodt,otp,fopd,otg,sxw,stw,sxg,sdw,sdc,sda,sdd,sdp,sgl,sxi,sti,vor
                [abiword]=abw,zabw
                  [video]=ogv,mp4,wmv,avi,mpeg,mpeg4,mpe,mpg,mkv,webm
                  [audio]=ogg,mp3,wma,wav,wave,mid,midi,au,ct2
                  [score]=enc,mus,mts,musx,rmi,wxmx
                   [font]=fot,ttf,otf,fon,eot,woff,afm,pfb,pcf,ttc,ots,ott
                [package]=deb,rpm,msi
                   [disk]=dd,i,img,iso,dsk,vmdk,vdi,vhd,vhdx,hdd,qed,qcow,qcow2
               [compress]=zip,gz,xz,bz2,tar,tgz,tbz,tbz2,jar,arj,lzh,lha,rar,shar,kmz
                [program]=class,exe,com
                [library]=dll,so
                   [misc]=rom,bin,swf
             )
    echo "*.{$(tr \  , <<< ${bintypes[*]})} filter=lfs diff=lfs merge=lfs -text lockable" >| .gitattributes



    # .gitignore
    cat << EOT >| .gitignore
# .gitignore

# - blank lines are ignored
# - lines beginning with '#' are ignored
# - standard glob patterns work (.a, .o)
# - can specify root directory files like '/TODO'
# - end patterns with a slash to specify a directory (bin/)
# - negate a pattern by beginning it with an '!'

# binary/bytecode files
*.[oa]
*.pyc
*.class

# ansible retry files
*.retry

# temporary files
*~

# vim swap files
# find . -name "*.sw?" -exec rm "{}" \;
.*.sw?

# renamed git control directories
_git/

# EOF
EOT



    # .gitmake (custom)
    cat << EOT >| .gitmake
# vim: ft=sh

# .gitmake

    namespace=$_namespace
      project=$_project
       branch=$_branch
  description="$_description"
     icon_src=$_icon_src

  ngp \$namespace/\$project:\$branch \$icon_src \$description do

  if cd \$project
  then

    git branch -a

  fi

# EOF
EOT

    # .gitedit (custom)
    cat << EOT >| .gitedit
#!/bin/bash

  _V=$_project # vim group label

  vi .gitmake
  vi README.md
# vi Dockerfile
# vi .gitlab-ci.yml
# vi docker-compose.yml  docker-comp
# vi rancher-compose.yml rancher-comp

# EOF
EOT
    chmod +x .gitedit

}



# EOF
