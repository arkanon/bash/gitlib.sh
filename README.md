# gitlib.sh

Procedimento para automação em shell, por uso/configuração da linha de comando e pela API, das principais características do GitLab.  
Projeto criado como demonstração em <<http://gitlab.com/arkanon/gitlib-demo>>.



### TODO

  - `ok` criação do token pessoal de API
  - `ok` envio de chave SSH pública para o servidor GitLab
  - `ok` criação dos arquivos de controle
  - `ok` criação do projeto
  - `ok` definição da descrição do projeto
  - `ok` definição do avatar (icone) em url
  - `ok` definição do avatar (icone) caminho local
  - `ok` atualização da descrição
  - `ok` alteração do nome do projeto
  - `ok` remoção automática dos arquivos auxiliares / repo local / repo remoto / projeto
  - `ok` múltiplas identidades conforme servidor git
  - `ok` gerenciamento de variáveis
  - `ok` gerenciamento de branches
  - `ok` git aliases
  - `ok` acompanhamento de pipelines
  - `ok` repositório em qualquer namespace acessível
<!-- -->
  - `--` escapar sinais de < e > em url's
  - `--` git config
  - `--` git aliases
<!-- -->
  - `--` converter este arquivo em markdown
  - `--` relação entre os comandos/procedimentos e os locais na interface web
<!-- -->
  - `--` variáveis
  - `--` branches
  - `--` merge requests
  - `--` merge
  - `--` merge rollback
  - `--` stash
  - `--` tags
  - `--` CI
<!-- -->
  - `--` "clonar" apenas um diretório de um projeto sem trazer junto seu histórico (.git/)
  - `--` extrair arquivos/diretórios de um projeto como novo projeto levando seu histórico junto
  - `--` transformar um subdiretório de um repositório em um novo repositório
  - `--` alterar a mensagem de commit
  - `--` autenticação HTTPS por token OAuth2
  - `--` clonar e atualizar apenas subdiretórios de um repositório
  - `--` usar projeto como módulo de outro
  - `--` manter repositório sincronizado em múltiplos servidores git
  - `--` usar git (que suporta links simbólicos) como serviço de auto sync tipo dropbox (que não suporta links simbólicos)



### Sumário

- inclusão da biblioteca
- inclusão dos parâmetros mais padrão
- instalação de ferramentas auxiliares
- criação do token pessoal de API
- envio de chave SSH pública para o servidor GitLab
- criação e clonagem inicial do projeto
- atualização da descrição
- alteração do nome do projeto
- gerenciamento de variáveis
- remoção automática dos arquivos auxiliares / repo local / repo remoto / projeto



```sh

  . gitlib.sh
  . gitlib.conf

  sudo apt -y install tidy        # formatação de x(ht)ml
  sudo apt -y install putty-tools # puttygen
  sudo apt -y install xclip       # envio por linha de comando de conteúdo para a área de transferência

  create_token # <http://$gl_host/profile/personal_access_tokens>
  send_key     # <http://$gl_host/profile/keys>



  mkdir -p $HOME/git
  cd $HOME/git

      project=gitlab-api-demo
  description="Projeto de demonstração criado pelo procedimento de exploração por linha de comando e API das principais caracteristicas do GitLab."
     icon_src=https://ybug.io/images/integrations/square/gitlab.svg

  ngp $project $icon_src $description go
  cd  $project
  git branch



  # >> atualização da descrição <<
# api1 PUT /projects/$namespace%2F$project -F description="$description"



  # >> alteração do nome do projeto <<
  old=git-api
  new=gitlab-api
  api1 PUT /projects/$namespace%2F$old -F name=$new -F path=$new
  cd .. && mv $old $new && cd $new
  git remote set-url origin git@$gl_host:$namespace/$new



  # >> variáveis <<
  # lista
  api1 GET /projects/$namespace%2F$project/variables
  api2var $namespace/$project

  # define
  api1 POST /projects/$namespace%2F$project/variables \
    -F           key=VAR1 \
    -F         value=VAL1 \
    -F variable_type=env_var \
    -F     protected=false \
    -F        masked=false
  api2var $namespace/$project VAR1 'new VAL1'
  api2var $namespace/$project VAR2 VAL2

  # lista
  api1 GET /projects/$namespace%2F$project/variables/VAR2
  api2var $namespace/$project VAR2
  api2var $namespace/$project | jq '.[] | "\(.key)=\(.value)"' | sed -r 's/^"//; s/=/="/' | GREP_COLOR='1;33' grep --color=always -E '|^[^=]+' | GREP_COLOR='0;41' grep --color=always -E '|='

  # delete
  api1 DELETE /projects/$namespace%2F$project/variables/VAR1
  api2var $namespace/$project VAR2-



  # >> remoção do projeto no servidor do GitLab e do clone local <<
  cd ..
# remove_all


```



```
# EOF
```
